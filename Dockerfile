FROM node:10
RUN npm install npm@latest -g
RUN npm install http-server -g
ADD . /app
WORKDIR /app
CMD http-server build -p 8003
